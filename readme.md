# SLAMdog Repository
This repository contains a ROS 1 (Noetic) workspace for simultaneous localization and mapping (SLAM) using the real LiDAR Sensor A1M8 from SLAMTEC.

NOTE: This Repository is a code collection. We did not write the packages ourselves! So the licences of the packages must be checked!

## Installation
- Make sure _ROS 1 Noetic (Full Desktop)_ is installed on your system
- Install the _MRPT SLAM LIB_ - An instruction is given [here](https://docs.mrpt.org/reference/latest/download-mrpt.html#downloadmrpt)
- Install the ROS [MRPT SLAM Packages](http://wiki.ros.org/mrpt_icp_slam_2d) with `sudo apt install ros-noetic-mrpt-*`
- Clone this repository `git clone https://gitlab.com/christian-stamm/slamdog.git`
- Navigate into the repository by typing `cd slamdog`
- Init git submodules `git submodule init`
- Update git submodules `git submodule update --recursive`
- Build the code by running `catkin_make` (in the root path of this repository)
- Source the built ROS Packages `source devel/setup.sh`

Congrats the installation should be completed!

## Running the Application
- Make sure all ROS Packages are sourced in the current Terminal
- Start the ROS applcation with `roslaunch icp_slam icp_slam.launch`
- An RVIZ Window should open and display:
    - The current raw pointcloud
    - A map build of the set of pointclouds
    - The sensor pose inside the map
    - A trajectory of the sensor movement inside the map
- To store the generated map run the command `rosrun map_server map_saver -f <filename>`
